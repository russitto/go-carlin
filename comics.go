package main

import (
	"fmt"
	"time"
)

const URL_COMIC = "http://www.gocomics.com/espanol/%s/%s"

var comics = [4]string{
	"garfieldespanol",
	"tutelandia",
	"wumoespanol",
	"dilbert-en-espanol",
}

func SendComics(from int) {
	now := time.Now()
	date := fmt.Sprintf("%d/%d/%d", now.Year(), now.Month(), now.Day())
	for i := 0; i < len(comics); i++ {
		url := fmt.Sprintf(URL_COMIC, comics[i], date)
		SendMessage(url, from, "")
	}
}
