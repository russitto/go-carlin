package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Meme struct {
	Id   int
	Name string
}
type Memes []Meme

type ImgflipBody struct {
	Success bool
	Data    struct {
		Url string
	}
}

var memes map[string]Meme = parseMemes()

func SendMemeSuarez(from int, text string) {
	text = regexp.MustCompile(`a|e|o|u`).ReplaceAllString(text, "i")
	SendMeme(from, "suarez "+text)
}

func SendMeme(from int, text string) string {
	spl := strings.Split(text, " ")
	if len(spl) == 0 {
		return ""
	}
	key, text := spl[0], strings.Join(spl[1:], " ")
	m := memes[key]
	if m.Id == 0 || text == "" {
		return ""
	}
	lines := strings.Split(text, "_")
	if len(lines) == 0 {
		return ""
	}
	values := url.Values{
		"template_id": {strconv.Itoa(m.Id)},
		"username":    {Conf.Meme.User},
		"password":    {Conf.Meme.Pass},
		"text0":       {lines[0]},
	}
	if len(lines) > 1 {
		values.Add("text1", lines[1])
	}
	resp, err := http.PostForm(Conf.Meme.Url, values)
	if err != nil {
		log.Println("Error posting to imgflip ", err)
		return ""
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Println("Error closing imgflip response body ", err)
			return
		}
	}()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error parsing imgflip response body ", err)
	}

	var dat ImgflipBody
	if err = json.Unmarshal(body, &dat); err != nil {
		log.Println("Error parsing imgflip response body ", err)
		return ""
	}
	if !dat.Success {
		log.Println("Imgflip response success false")
		return ""
	}

	img := dat.Data.Url

	if from != 0 {
		SendMessage(img, from, "")
		return ""
	}
	return img
}

func SendMemes(from int) {
	out := make([]string, len(memes))
	i := 0
	for k, _ := range memes {
		out[i] = k
		i++
	}
	SendMessage(strings.Join(out, " "), from, "")
}

func SendMessage(text string, from int, parseMode string) {
	teleUrl := fmt.Sprintf("%s%s/sendMessage", Conf.Telegram.Url, Conf.Telegram.Token)
	values := url.Values{
		"chat_id": {strconv.Itoa(from)},
		"text":    {text},
	}
	if parseMode != "" {
		values.Add("parse_mode", parseMode)
	}
	resp, err := http.PostForm(teleUrl, values)
	if err != nil {
		log.Println("Error posting to telegram api ", err)
		return
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Println("Error closing imgflip telegram api body ", err)
			return
		}
	}()
}

func SendSlack(text string) string {
	out := fmt.Sprintf("{\"response_type\": \"in_channel\", \"text\": \"%s\"}", text)
	return out
}

func SendSlackMrkdwn(text string) string {
	out := fmt.Sprintf("{\"response_type\": \"in_channel\", \"mrkdwn\": true, \"text\": \"```\\n%s```\"}", text)
	return out
}

func SendSlackImg(text string) string {
	out := fmt.Sprintf("{\"response_type\": \"in_channel\", \"attachments\": [{\"image_url\": \"%s\"}]}", text)
	return out
}

func Hello() string {
	return "mmmmmm " + Conf.Listening
}

func parseMemes() map[string]Meme {
	file, err := os.Open("memes.json")

	if err != nil {
		log.Fatal("Error reading memes.json file", err)
	}
	decoder := json.NewDecoder(file)
	memesSl := Memes{}

	err = decoder.Decode(&memesSl)
	if err != nil {
		log.Fatal("Error parsing memes.json file", err)
	}

	defer func() {
		err := file.Close()
		if err != nil {
			log.Fatal("Error closing memes.json file", err)
		}
	}()

	out := make(map[string]Meme)
	for i := 0; i < len(memesSl); i++ {
		out[memesSl[i].Name] = memesSl[i]
	}
	return out
}
