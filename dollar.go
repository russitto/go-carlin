package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
)

const DOLAR_HOY = "http://www.dolarhoy.com/"

var re_DOLAR_SELL = regexp.MustCompile(`<h4 class="pull-left">[a-zA-Z]+ <span class="pull-right">\$ ([0-9\.,]+)<\/span><\/h4>`)

func SendDollar(from int) string {
	buy, sell, err := dollar()
	if err != nil {
		return ""
	}
	out := fmt.Sprintf("buy: ARS %s - sell: ARS %s", buy, sell)
	if from != 0 {
		SendMessage(out, from, "")
		return ""
	}
	return out
}

func dollar() (string, string, error) {
	req, err := http.Get(DOLAR_HOY)
	if err != nil {
		return "", "", err
	}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return "", "", err
	}
	sbody := string(body)
	sellLine := re_DOLAR_SELL.FindAllString(sbody, 2)
	if len(sellLine) < 2 {
		return "", "", errors.New("Dollar::empty values")
	}
	buy := re_DOLAR_SELL.ReplaceAllString(sellLine[0], "$1")
	sell := re_DOLAR_SELL.ReplaceAllString(sellLine[1], "$1")
	return buy, sell, nil
}
