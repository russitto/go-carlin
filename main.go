package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
)

type Configuration struct {
	Listening string
	Telegram  struct {
		Token       string
		Url         string
		Alias       string
		Bbhookgroup int
	}
	Meme struct {
		Url  string
		User string
		Pass string
	}
}

var Conf Configuration

type TeleObj struct {
	Message struct {
		Message_id int
		From       struct {
			Id       int
			Username string
		}
		Chat struct {
			Id       int
			Username string
			Type     string
		}
		Text string
	}
}

var TeleAliasRe = regexp.MustCompile("$")

func main() {
	file, err := os.Open("config.json")

	if err != nil {
		log.Fatal("Error reading config.json file", err)
	}
	decoder := json.NewDecoder(file)
	Conf = Configuration{}

	err = decoder.Decode(&Conf)
	if err != nil {
		log.Fatal("Error parsing config.json file", err)
	}

	defer func() {
		err := file.Close()
		if err != nil {
			log.Fatal("Error closing config.json file", err)
		}
	}()
	if Conf.Telegram.Token == "" {
		Conf.Telegram.Token = os.Getenv("TELE_TOKEN")
	}
	if Conf.Meme.User == "" {
		Conf.Meme.User = os.Getenv("MEME_USER")
	}
	if Conf.Meme.Pass == "" {
		Conf.Meme.Pass = os.Getenv("MEME_PASS")
	}
	if Conf.Listening == "" {
		Conf.Listening = ":" + os.Getenv("PORT")
	}

	if FortuneImport() != nil {
		log.Fatal("Error importing fortune files", err)
	}

	// BEGIN just one test
	// SendSubway(0)
	// END just one test
	TeleAliasRe = regexp.MustCompile(Conf.Telegram.Alias + "$")

	http.HandleFunc("/bbhook", Bbhook)
	http.HandleFunc("/telegram", Telegram)
	http.HandleFunc("/slack", Slack)
	http.HandleFunc("/", Index)

	log.Printf("Listening on: %s\n", Conf.Listening)
	log.Fatal(http.ListenAndServe(Conf.Listening, nil))
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome!\n")
}

func Telegram(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" || r.Header.Get("Content-Type") != "application/json" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprint(w, "Method or content type not allowed.\n")
		return
	}
	var t TeleObj
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&t)
	if err != nil {
		fmt.Fprint(w, err.Error())
		return
	}

	text := strings.TrimSpace(t.Message.Text)
	spl := strings.Split(text, " ")
	if text == "" || len(spl) == 0 {
		fmt.Fprint(w, "Empty message.\n")
		return
	}
	command, text := spl[0], strings.Join(spl[1:], " ")
	private := t.Message.Chat.Type == "private"
	from := t.Message.Chat.Id
	if from == 0 {
		from = t.Message.From.Id
	}
	user := t.Message.From.Username
	Message(command, text, user, from, private)
}

func Slack(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// @TODO catch ParseForm possible error
	r.ParseForm()

	switch r.PostForm.Get("command") {
	case "/sub":
		fmt.Fprint(w, SlackSub())
	case "/meme":
		text := r.PostForm.Get("text")
		fmt.Fprint(w, SendSlackImg(SendMeme(0, text)))
	case "/temp":
		text := r.PostForm.Get("text")
		fmt.Fprint(w, SendSlack(SendWeather(0, text)))
	case "/dollar":
		fmt.Fprint(w, SendSlack(SendDollar(0)))
	case "/coins":
		fmt.Fprint(w, SendSlackMrkdwn(SendCoins(0)))
	case "/hn":
		fmt.Fprint(w, SendSlackMrkdwn(SendHn(0)))
	case "/rss":
		text := r.PostForm.Get("text")
		fmt.Fprint(w, SendSlackMrkdwn(SendRss(0, text)))
	default:
		fmt.Fprint(w, "Mandaste frula\n")
	}
}

func Message(command, text, user string, from int, private bool) {
	log.Println("command", command)
	command = TeleAliasRe.ReplaceAllString(command, "")
	switch command {
	case "/meme":
		SendMeme(from, text)
	case "/suarez":
		SendMemeSuarez(from, text)
	case "/memes":
		SendMemes(from)
	case "/temp":
		SendWeather(from, text)
	case "/sub":
		SendSubway(from)
	case "/comics":
		SendComics(from)
	case "/prensa":
		SendPress(from, text)
	case "/slaps":
		SendSlaps(from, text)
	case "/coins":
		SendCoins(from)
	case "/dollar":
		SendDollar(from)
	case "/hn":
		SendHn(from)
	case "/rss":
		SendRss(from, text)
	}
	return
}
