package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strings"
)

const HN_URL = "https://api.hackerwebapp.com/news"
const HN_PREF = "https://news.ycombinator.com/"

type HnItem struct {
	Id             int
	Url            string
	Title          string
	Points         int
	Comments_count int
	Time_ago       string
}

func SendHn(from int) string {
	news, err := hackerNews()
	if err != nil {
		return ""
	}
	if from != 0 {
		SendMessage(news, from, "")
		return ""
	}
	return news
}

func hackerNews() (string, error) {
	req, err := http.Get(HN_URL)
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return "", err
	}

	items := make([]HnItem, 0)
	err = json.Unmarshal(body, &items)
	if err != nil {
		return "", err
	}

	sort.Slice(items, func(i, j int) bool {
		return items[i].Points > items[j].Points
	})
	var sb0 strings.Builder
	// var sb1 strings.Builder
	// TOP 15
	for i := 0; i < 15 && i < len(items); i++ {
		// if i > 0 {
		// 	sb.WriteString("--------------------\n")
		// }
		url := items[i].Url
		if strings.Index(url, "http") != 0 {
			url = fmt.Sprintf("%s%s", HN_PREF, url)
		}
		sb0.WriteString(fmt.Sprintf("[P %d] %s %s (C %d)\n", items[i].Points, url, items[i].Title, items[i].Comments_count))
	}
	// for i := 15; i < len(items); i++ {
	// 	sb1.WriteString(fmt.Sprintf("[P %d] %s %s (C %d)\n", items[i].Points, items[i].Url, items[i].Title, items[i].Comments_count))
	// }
	// return sb0.String(), sb1.String(), nil
	return sb0.String(), nil
}
