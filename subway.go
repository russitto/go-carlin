package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"sort"
	"strings"
)

var metroLines = [...]rune{'A', 'B', 'C', 'D', 'E', 'H', 'P', 'U'}
var metroUrl = "https://www.metrovias.com.ar/"

var reMetroLine = regexp.MustCompile(".*id=\"status-line-.\">(.*)<\\/span>")

func SendSubway(from int) {
	status, err := metrovias()
	if err != nil {
		fmt.Println("Error gettin metrovias", err)
		return
	}
	msg := make([]string, len(status))
	i := 0
	for k, v := range status {
		msg[i] = fmt.Sprintf("%c ➡️  %s", k, v)
		i++
	}
	sort.Strings(msg)
	out := fmt.Sprintf("<pre>%s</pre>", strings.Join(msg, "\n"))
	SendMessage(out, from, "HTML")
}

func SlackSub() string {
	status, err := metrovias()
	if err != nil {
		fmt.Println("Error gettin metrovias", err)
		return ""
	}
	msg := make([]string, len(status))
	i := 0
	for k, v := range status {
		msg[i] = fmt.Sprintf("%c ➡️  %s", k, v)
		i++
	}
	sort.Strings(msg)
	return SendSlackMrkdwn(strings.Join(msg, "\\n"))
}

func metrovias() (map[rune]string, error) {
	out := map[rune]string{}
	resp, err := http.Get(metroUrl)

	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	lineRows := reMetroLine.FindAllString(string(body), -1)
	for i := 0; i < len(lineRows) && i < len(metroLines); i++ {
		state := reMetroLine.ReplaceAllString(lineRows[i], "$1")
		out[metroLines[i]] = state
	}
	return out, nil
}
