package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strconv"
)

const WOEID_BSAS = "468739"
const URL_BSAS = "https://query.yahooapis.com/v1/public/yql?q=select%%20item.condition.temp%%2C%%20item.condition.text%%2C%%20atmosphere.humidity%%20from%%20weather.forecast%%20where%%20woeid%%20%%3D%s%%20%%20&format=json&env=store%%3A%%2F%%2Fdatatables.org%%2Falltableswithkeys"

type Weather struct {
	Query struct {
		Results struct {
			Channel struct {
				Atmosphere struct {
					Humidity string
				}
				Item struct {
					Condition struct {
						Temp string
						Text string
					}
				}
			}
		}
	}
}

func SendWeather(from int, message string) string {
	if message == "" {
		message = WOEID_BSAS
	}
	url := fmt.Sprintf(URL_BSAS, message)
	resp, err := http.Get(url)
	if err != nil {
		log.Println("Error getting from yahoo weather ", err)
		return ""
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Println("Error closing imgflip response body ", err)
			return
		}
	}()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error parsing yahoo weather response body ", err)
	}
	var dat Weather
	if err = json.Unmarshal(body, &dat); err != nil {
		log.Println("Error parsing yahoo weather response body ", err)
		return ""
	}
	condition := dat.Query.Results.Channel.Item.Condition.Text
	temp, err := strconv.Atoi(dat.Query.Results.Channel.Item.Condition.Temp)
	if err != nil {
		log.Println("Error parsing yahoo weather response body, bad temperature ", err)
		return ""
	}
	humidity, err := strconv.Atoi(dat.Query.Results.Channel.Atmosphere.Humidity)
	if err != nil {
		log.Println("Error parsing yahoo weather response body, bad humidity ", err)
		return ""
	}
	// to celsius:
	temp = int(math.Round((float64(temp) - 32) / 1.8))

	msg := fmt.Sprintf("%s %d° %d%% hum.", condition, temp, humidity)
	if from != 0 {
		SendMessage(msg, from, "")
		return ""
	}
	return msg
}
